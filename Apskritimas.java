package com.bta;

public class Apskritimas extends Figura {

    private static double pi = 3.14;
    private double skersmuo;
    private double spindulys;

    Apskritimas(double spindulys) {
        this.spindulys = spindulys;
        this.skersmuo = spindulys * 2;
    }

    public double plotas() {
        // I static kintamaji kreipiames klasesPavadinimas.kintamasis
        return 2 * Apskritimas.pi * this.spindulys;
    }

    public double perimetras() {
        return this.skersmuo * Apskritimas.pi;
    }

}
