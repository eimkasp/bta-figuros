package com.bta;

public class Staciakampis extends Figura{
    private double krastine1;
    private double krastine2;

    Staciakampis(double krastine1, double krastine2) {
        this.krastine1 = krastine1;
        this.krastine2 = krastine2;
    }

    public double plotas() {
        double plotas = this.krastine1 * this.krastine2;
        return plotas;
    }

    public double perimetras() {
        return krastine2 * 2 + krastine1 * 2;
    }

}
