package com.bta;

public abstract class Figura {

    public abstract double plotas();

    public abstract double perimetras();
}
