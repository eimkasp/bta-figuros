package com.bta;

public class Main {

    public static void main(String[] args) {
	// write your code here


        Apskritimas a1 = new Apskritimas(3);
        System.out.println("Plotas: " + a1.plotas());
        System.out.println("Perimetras: " + a1.perimetras());


        Staciakampis st1 = new Staciakampis(4, 9);
        System.out.println("Staciakampio Plotas: " + st1.plotas());
        System.out.println("Staciakampio Perimetras: " + st1.perimetras());
    }
}
